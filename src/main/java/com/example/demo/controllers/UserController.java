package com.example.demo.controllers;

import com.example.demo.models.ResponseObject;
import com.example.demo.models.User;
import com.example.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/users")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    @GetMapping("")
    List<User> getUsers() {
        return userRepository.findAll();
    }

    @PostMapping("/insert")
    ResponseEntity<ResponseObject> insert(@RequestBody User newUser) {
        List<User> foundUser = userRepository.findByUsername(newUser.getUsername().trim());
        return !foundUser.isEmpty() ? ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                new ResponseObject("failed", "User name already taken", "")
        ) : ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Insert success", userRepository.save(newUser))
        );
    }
}

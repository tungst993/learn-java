package com.example.demo.controllers;

import com.example.demo.models.Product;
import com.example.demo.models.ResponseObject;
import com.example.demo.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;
    @GetMapping("getAll")
    List<Product> getAllProducts() {
        return (List<Product>) productRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<ResponseObject> findById(@PathVariable Long id) {
        Optional<Product> foundProduct = productRepository.findById(id);
            return foundProduct.isPresent() ? ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Query product successfully", foundProduct)
            ) : ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                    new ResponseObject("error", "Not found id = " + id, "")
            );
    }

    @PostMapping("/insert")
    ResponseEntity<ResponseObject> insert(@RequestBody Product newProduct) {
        List<Product> foundProduct = productRepository.findByProductName(newProduct.getProductName().trim());

        return !foundProduct.isEmpty() ?  ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                new ResponseObject("failed", "Product name already taken", "")
        ) :  ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Insert successfully", productRepository.save(newProduct))
        );

    }

    @PutMapping("/{id}")
    ResponseEntity<ResponseObject> update(@RequestBody Product newProduct, @PathVariable Long id) {
       Product updateProduct = productRepository.findById(id).map(product -> {
            product.setProductName(newProduct.getProductName());
            product.setPrice(newProduct.getPrice());
            product.setUrl(newProduct.getUrl());
            return productRepository.save(product);
        }).orElseGet(() -> {
            newProduct.setId(id);
            return productRepository.save(newProduct);
        });

        return  ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Insert successfully", productRepository.save(newProduct))
        );
    }

    @DeleteMapping("/{id}")
    ResponseEntity<ResponseObject> deleteProduct(@PathVariable Long id) {
        boolean exists = productRepository.existsById(id);
        if(exists) {
            productRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "Delete product successfully", "")
            );
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                new ResponseObject("fail", "Can not find product", "")
        );
    }
}